FROM openjdk:17-jdk-alpine

ARG APP_VERSION
ENV APP_VERSION $APP_VERSION

RUN echo $APP_VERSION

RUN mkdir logs
VOLUME /logs
VOLUME /tmp

COPY target/rest-api-$APP_VERSION.jar app.jar

# ENV JAVA_OPTS="-Xmx=1024m"

# ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
ENTRYPOINT exec java $JAVA_OPTS -jar /app.jar