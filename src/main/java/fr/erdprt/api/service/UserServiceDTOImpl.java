package fr.erdprt.api.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import fr.erdprt.api.model.UserDTO;



@Service("userServiceDTO")
public class UserServiceDTOImpl implements UserServiceDTO{
	
	private static final AtomicLong counter = new AtomicLong();
	
	private static List<UserDTO> users;
	
	static{
		users= populateDummyUsers();
	}

	@Override
	public List<UserDTO> findAllUsers() {
		return users;
	}
	
	@Override
	public UserDTO findById(long id) {
		for(UserDTO user : users){
			if(user.getId() == id){
				return user;
			}
		}
		return null;
	}
	
	@Override
	public UserDTO findByName(String name) {
		for(UserDTO user : users){
			if(user.getName().equalsIgnoreCase(name)){
				return user;
			}
		}
		return null;
	}
	
	@Override
	public void saveUser(UserDTO user) {
		user.setId(counter.incrementAndGet());
		users.add(user);
	}

	@Override
	public void updateUser(UserDTO user) {
		int index = users.indexOf(user);
		users.set(index, user);
	}

	@Override
	public void deleteUserById(long id) {
		
		for (Iterator<UserDTO> iterator = users.iterator(); iterator.hasNext(); ) {
		    UserDTO user = iterator.next();
		    if (user.getId() == id) {
		        iterator.remove();
		    }
		}
	}

	@Override
	public boolean isUserExist(UserDTO user) {
		return findByName(user.getName())!=null;
	}
	
	@Override
	public void deleteAllUsers(){
		users.clear();
	}

	private static List<UserDTO> populateDummyUsers(){
		List<UserDTO> users = new ArrayList<UserDTO>();
		users.add(new UserDTO(counter.incrementAndGet(),"Sam",30, 70000));
		users.add(new UserDTO(counter.incrementAndGet(),"Tom",40, 50000));
		users.add(new UserDTO(counter.incrementAndGet(),"Jerome",45, 30000));
		users.add(new UserDTO(counter.incrementAndGet(),"Silvia",50, 40000));
		return users;
	}

}
