package fr.erdprt.api.service;

import java.util.List;
import fr.erdprt.api.model.UserDTO;

public interface UserServiceDTO {
	
	UserDTO findById(long id);
	
	UserDTO findByName(String name);
	
	void saveUser(UserDTO user);
	
	void updateUser(UserDTO user);
	
	void deleteUserById(long id);

	List<UserDTO> findAllUsers();
	
	void deleteAllUsers();
	
	boolean isUserExist(UserDTO user);
	
}
