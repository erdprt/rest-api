package fr.erdprt.api.repository;

public interface AppRepository {
	
	  long countProductsWithVersion();

	  String findNameById(int id);

	}