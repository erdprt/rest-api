CREATE TABLE IF NOT EXISTS countries (
  id   INTEGER NOT NULL AUTO_INCREMENT,
  iso char(2) NOT NULL,
  name VARCHAR(128) NOT NULL,
  nicename varchar(80) NOT NULL,
  iso3 char(3) DEFAULT NULL,
  numcode smallint(6) DEFAULT NULL,
  phonecode int(5) NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE countries_seq START WITH 300;

CREATE TABLE IF NOT EXISTS users (
  id INTEGER NOT NULL,
  id_country INTEGER NOT NULL,  
  firstname VARCHAR(50) NOT NULL,
  lastname varchar(50) NOT NULL,
  birthdate Date NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE users
    ADD FOREIGN KEY (id_country) 
    REFERENCES countries(id);
    
CREATE SEQUENCE users_seq START WITH 10;    