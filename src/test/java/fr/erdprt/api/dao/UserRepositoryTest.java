package fr.erdprt.api.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.erdprt.Application;
import fr.erdprt.api.model.Country;
import fr.erdprt.api.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserRepositoryTest {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Test
	public void findAll() {
		List<User> list = new ArrayList<User>();
		this.userRepository.findAll().forEach(list::add);
		Assert.assertEquals(5, list.size());
	}

	@Test
	public void save() throws ParseException {
		Country country = countryRepository.findById(1).get();
		
		User user = new User("FN", "LN", new SimpleDateFormat("dd-MM-yyyy").parse("12-01-1941"), country);
		this.userRepository.save(user);
		
		Assert.assertNotNull(user);
		Assert.assertNotNull(user.getId());
		
		Optional<User> newUser =  this.userRepository.findById(user.getId());
		Assert.assertNotNull(newUser.get());
		Assert.assertEquals(newUser.get().getFirstName(), user.getFirstName());
		
		List<User> list = new ArrayList<User>();
		this.userRepository.findAll().forEach(list::add);
		Assert.assertEquals(6, list.size());
	}

}
